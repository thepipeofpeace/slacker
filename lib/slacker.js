var ManyFacedBot = require("./many_faced_bot.js");

// Slacker is a being, an identity for Many Faced Bot.
function Slacker(conf, pulse) {
  var self = this;

  // A client of Many Faced Bot API.
  self.manyFacedBot = new ManyFacedBot(
    conf.MANY_FACED_BOT_URL,
    conf.MANY_FACED_BOT_ACCESS_TOKEN,
    pulse
  );

  // Handles all direct messages and mentions of our Bot.
  var onDirectMessage = function (bot, message) {
    self.manyFacedBot.listen(message, function (response) {
      bot.reply(message, response);
    });
  }

  // Initializes an instance of identity for Many Faced Bot. It configures all
  // necessary message handlers.
  self.init = function (_bot, ctrl) {
    // TODO: Initialize with bot name.

    ctrl.hears(['.*'], ['direct_message', 'direct_mention'], onDirectMessage);
  }
}

module.exports = Slacker;
