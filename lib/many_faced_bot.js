var unirest  = require("unirest")
  , attempt  = require("attempt")
  , messages = require("./messages.js");

// Very simple client for Many Faced Bot APIs.
function ManyFacedBot(url, token, pulse) {
  var self = this;

  // Related metric.
  var metric = pulse("order");

  // Retry on error attempt settings.
  var attemptConfig = {
    retries:  4,
    factor:   1.6,
    interval: 2000
  };

  // Error handler for failed attempts.
  attemptConfig.onError = function (err) {
    metric("error").inc();
    console.error(err);
  };

  // List of default HTTP headers sent to Many Faced Bot API.
  var defaultHeaders = {
    'Accept': 'application/json',
    'Authorization': 'Bearer ' + token,
  };

  // Performs single configured request agains Many Faced Bot API.
  var request = function (path, data) {
    return unirest
      .post(url + path)
      .type('application/json')
      .headers(defaultHeaders)
      .send(data);
  };

  // Here we process all orders sent to the Bot. All mentions and direct
  // messages are transformed into orders sent against the Many Faced Bot API.
  var order = function (message, callback) {
    var req = request("/orders", { order: message.text })
    req.end(function (response) {
      if (response.code == 200) {
        var answers = response.body;
        answers.forEach(function(answer, _, _) {
          callback(null, answer);
        });
      } else {
        callback(new Error("Failed to get an answer from Many Faced Bot!"));
      }
    });
  };

  // Safe version of sending an order.
  self.listen = function (message, callback) {
    attempt(attemptConfig,
      function (attempts) {
        var callback = this;
        metric("attempt").inc().time(function () {
          order(message, callback);
        });
      },
      function (err, result) {
        if (err) {
          metric("unreachable").inc();
          callback(messages.errors.many_faced_bot_unreachable);
        } else {
          metric("success").inc();
          callback(result);
        }
      }
    );
  };
}

module.exports = ManyFacedBot;
