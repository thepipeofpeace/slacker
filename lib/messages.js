var messages = {
  errors: {
    many_faced_bot_unreachable:
      "Yaiks! Many Faced Bot seems to be unreachable. " +
      "Would you mind trying in a few minutes?"
  }
};

module.exports = messages;
