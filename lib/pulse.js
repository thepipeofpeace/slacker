var dgram = require("dgram");

// Pulse is a fancy, yet ultimately simple metrics client. It only hands over
// delta messages, analysis is left to be handlend in metrics collector.
function Pulse(conf) {
  var self = this;

  // Fix port (just in case).
  conf.port = parseInt(conf.port);

  // Underlying UDP client.
  var client = dgram.createSocket("udp4");

  // Sends delta operation using underlying UDP client.
  var send = function (cmd, key, value) {
    var message = new Buffer(cmd + " " + key + " " + value + "\n");
    client.send(message, 0, message.length, conf.port, conf.host);
  }

  // Returns current time.
  var now = function () {
    return new Date().getTime();
  }

  // An object wrapper for single metric.
  function Metric(key) {
    var self = function (childKey) {
      return new Metric(key + "/" + childKey);
    }

    // Increment value of given key by 1.
    self.inc = function () {
      return self.incBy(1);
    };

    // Increment value of given key by specified size.
    self.incBy = function (size) {
      send("INC", key, size);
      return self;
    }

    // Decrement value of given key by 1.
    self.dec = function () {
      return self.decBy(1);
    };

    // Decrement value of given key by specified size.
    self.decBy = function (size) {
      send("DEC", key, size);
      return self;
    }

    // Set value of given key.
    self.set = function (value) {
      send("SET", key, size);
      return self;
    };

    // Measure execution time and send as separate metric.
    self.time = function (callback) {
      var start = now();
      callback();
      send("TIME", key, now() - start);
      return self;
    }

    return self;
  }

  // Get proxy object for a metric with given name.
  return function (key) {
    return new Metric(key);
  }
}

module.exports = Pulse;
