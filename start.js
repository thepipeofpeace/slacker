// start.js
//
// Entry point script. Establishes connection with configured slack bot and
// starts handler loop.

var Botkit  = require('botkit')
  , Slacker = require('./lib/slacker.js')
  , Pulse   = require('./lib/pulse.js')
  , conf    = process.env
  , ctrl    = Botkit.slackbot();

// Spawn a bot.
var bot = ctrl.spawn({ token: conf.SLACK_TOKEN });

// Set up metrics.
var pulse = new Pulse({
  host: conf.PULSE_HOST,
  port: conf.PULSE_PORT
});

// Initalize Slacker's identity.
var slacker = new Slacker(conf, pulse);
slacker.init(bot, ctrl);

// Let the conversation begin...
bot.startRTM(function(err, bot, payload) {
  if (err) {
    throw new Error('Could not connect to Slack!');
  }

  // TODO: Notify our supervisor that we're up!

  ctrl.log.notice("Bot alive!")
});
