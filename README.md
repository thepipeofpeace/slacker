# Slacker

**Slacker is a bot that can live and communicate on Slack**.

This is a transport that connects APIs of our bot being with users on Slack.
You can think of Slacker as of a phone that allows you to call our bot and ask
him to do something for you. You can also think of Slacker as of a _face_, one
of many of Many Faced Bot.

## Follow up

* [Documentation](docs/README.md).
