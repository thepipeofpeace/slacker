# Development Environment

## Configure

Create your development config file based on provided sample:

```
$ cp dev.env.sample dev.env
```

## Run

Start an instance of the container, by executing:

```
$ docker-compose up
```

## Debug

You can always hop into the box by running:

```
$ docker-compose run [service] /bin/sh
```

While on a bash session on the box, you can execute any script or command manually.
