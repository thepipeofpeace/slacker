# Deployment

## Caveats

This is a bot transport. Since an instance of this guy appears as a single
bot user on Slack, it can be deployed only once. Any duplicate instances
will cause in jobs being executed by all the instances.

On the other hand, bot can go down and up without disrupting rest of the
system. Bot transport outage in worst scenario will disable someone from
bot's assistance.
