# Configuration Variables

### `SLACK_TOKEN`

The token of Slack Bot User to connect with transmitter.

### `MANY_FACED_BOT_URL`

The URL to API of Many Faced Bot.

### `MANY_FACED_BOT_ACCESS_TOKEN`

Access token issued for Many Faced Bot API.

### `PULSE_HOST`

The address of pulse metrics server.

### `PULSE_PORT`

The port on which the pulse metrics server is listening.
