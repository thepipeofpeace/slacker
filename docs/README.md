## Getting started

1. [Dependencies](DEPENDENCIES.md),
2. [Configuration Variables](CONFIGURATION.md),
3. [Development Environment](DEVELOPMENT.md),
4. [Deployment](DEPLOYMENT.md).
