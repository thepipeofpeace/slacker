FROM mhart/alpine-node:6

ENV DIR "/app"
ADD . $DIR
WORKDIR $DIR
RUN npm install

CMD script/start
